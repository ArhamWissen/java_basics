package org.arham.practice.java;

import java.io.IOException;
import java.util.Scanner;

public class TakingInputFromTheUser {
    public static void main(String[] args) throws IOException {

//        System.out.println("Enter the number");
//        try{
//        int num = System.in.read();
//            System.out.println(num);
//        }catch(IOException e){
//            e.getLocalizedMessage();
//        }
//        ----------------------------------------------------------------------------
//        InputStreamReader reader = new InputStreamReader(System.in);
//        BufferedReader br = new BufferedReader(reader);
//
//        int num = Integer.parseInt(br.readLine());
//        System.out.println(num);
//
//        String str = br.readLine();
//        System.out.println(str);
//
//        br.close();
//        ----------------------------------------------------------------------------

        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        System.out.println("The number entered is : "+num);
    }
}
