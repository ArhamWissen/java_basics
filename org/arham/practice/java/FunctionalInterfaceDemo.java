package org.arham.practice.java;

import java.lang.FunctionalInterface;

@FunctionalInterface
interface FunctionalInterfaceImplementation{
    void show();
}

public class FunctionalInterfaceDemo {
    public static void main(String[] args) {
        FunctionalInterfaceImplementation fii = () -> System.out.println("In show");
        fii.show();
    }
}
