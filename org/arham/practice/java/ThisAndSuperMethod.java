package org.arham.practice.java;

class A{
    public A(){
        super();
        System.out.println("Inside A");
    }

    public A(int x){
        super();
        System.out.println("This is inside the parameterized constructor of A : "+x);
    }
}
class B extends A {
    public B(){
        super();
        System.out.println("Inside B");
    }

//    public B(int x){
//        super(x);
//        System.out.println("This is inside the parameterized constructor of B : "+x);
//    }
    public B(int x){
        this();
        System.out.println("This is inside the parameterized constructor of B : "+x);
    }
}

public class ThisAndSuperMethod {
    public static void main(String[] args) {
//        B obj = new B();
        B objParam = new B(5);
    }


}
