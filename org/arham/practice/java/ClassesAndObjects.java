package org.arham.practice.java;

class Calculator{
    public int sum(int a, int b){
        int sum = a+b;
        return sum;
    }
}

public class ClassesAndObjects {

    public static void main(String[] args) {

        Calculator calculator = new Calculator();
        System.out.println(new Calculator().sum(5,7));
        int a = 10;
        int b = 20;
        int result = calculator.sum(a,b);
        System.out.println("The sum of a and b is = "+result);
    }
}
