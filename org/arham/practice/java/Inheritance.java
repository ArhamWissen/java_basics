package org.arham.practice.java;

class A_Inheritance {
    public void show(){
        System.out.println("Inside show from A_Inheritance class");
    }

    public void config(){
        System.out.println("In a config function from A_Inheritance class");
    }

}
class B_Inheritance extends A_Inheritance {
    @Override
    public void show(){
        System.out.println("Inside show from B_Inheritance class");
    }
}
class C_Inheritance extends B_Inheritance {

}

public class Inheritance {
    public static void main(String[] args) {

        B_Inheritance obj = new B_Inheritance();
        obj.show();
        obj.config();

    }
}
