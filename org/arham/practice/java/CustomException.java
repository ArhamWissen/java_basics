package org.arham.practice.java;

class MyException extends Exception {
    public MyException(String str){
        super(str);
    }
}

public class CustomException {
    public static void main(String[] args) {

        try {

            int i = 9;
            int j = i / 0;
            System.out.println(j);
        } catch (ArithmeticException e) {
            System.out.println("Exception has occurred. (Division) " + e.getMessage() + " not possible");

//            e.printStackTrace() will print the exception message with the details of the exception.
//            Which means it will print the stack trace. Which method is calling which method. Etc
//            e.getMessage() will print the message to give you the highlight of why the exception occurred

            try {
                int a = 20;
                int b = 18;
                int c = b / a;

                if (c == 0)
                    throw new MyException("Cannot divide by 0");
            } catch (MyException f){
                System.out.println(f.getMessage());
            }
        }
    }
}
