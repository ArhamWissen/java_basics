package org.arham.practice.java;

class Mobile {
    String brand;
    int price;
    String network;
    static String name;

    public void show(){
        System.out.println("The brand is : "+brand);
        System.out.println("The price is : "+price);
        System.out.println("The network is : "+network);
        System.out.println("The name is : "+name);
    }

    public static void show1(Mobile mob){
        System.out.println("The brand is : "+mob.brand);
        System.out.println("The price is : "+mob.price);
        System.out.println("The network is : "+mob.network);
        System.out.println("The name is : "+name);
    }
}

public class StaticVariable {
    public static void main(String[] args) {
        Mobile.name = "SmartPhone";

        Mobile mob1 = new Mobile();
        mob1.brand = "Apple";
        mob1.price = 1000;
        mob1.network = "Airtel";

        Mobile mob2 = new Mobile();
        mob2.brand = "Samsung";
        mob2.price = 2000;
        mob2.network = "Jio";

        mob1.show();
        mob2.show();
        Mobile.show1(mob1);
    }
}
