package org.arham.practice.java;

import static java.lang.System.*;

public class TypeConversion {
    public static void main(String[] args) {
        byte b = 127;
        int a =258;
        byte k = (byte)a;

        float f = 5.6f;
        int t = (int)f;

        out.println("The value of b:"+b);
        System.out.println("The value of a:"+a);
        System.out.println("The value of k:"+k);
        System.out.println("The value of f:"+f);
        System.out.println("The value of t:"+t);
        System.out.println("The value of b*k:"+(b*k));



    }
}
