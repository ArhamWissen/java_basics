package org.arham.practice.java;
class ArhamA{
    public void show(){
        System.out.println("Inside show method of ArhamA ");
    }
}
class ArhamB extends ArhamA{
    @Override
    public void show(){
        System.out.println("Inside show method of ArhamB ");
    }
}
class ArhamC extends ArhamA{
    @Override
    public void show(){
        System.out.println("Inside show method of ArhamC ");
    }
}

public class DynamicMethodDispatch {

    public static void main(String[] args) {
        ArhamA obj = new ArhamA();
        obj.show();

        obj = new ArhamB();
        obj.show();

        obj = new ArhamC();
        obj.show();

    }
}
