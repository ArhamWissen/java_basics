package org.arham.practice.java;

abstract class ExampleForAbstraction{
    int age = 44;
    String city = "Bangalore";
    public abstract void show();
    public abstract void config();
    public void dontShow(){
        System.out.println("Not showing");
    }
}
interface ExampleForInterface{
    int age = 44;
    String city = "Bangalore";
    void show();
    void config();
}

interface MultipleInterface{
    void show();
    void config();
    void run();
}

class ImplementingInterface implements ExampleForInterface,MultipleInterface{
    int age = 44;
    String city = "Bangalore";
    @Override
    public void show() {
        System.out.println("This is from ImplementingInterface show");
    }

    @Override
    public void config() {
        System.out.println("This is from ImplementingInterface config");
    }

    @Override
    public void run() {
        System.out.println("This is from ImplementingInterface running");
    }
}
class ImplementingAbstraction extends ExampleForAbstraction{
    @Override
    public void show() {
        System.out.println("This is from ImplementingAbstraction");
    }

    @Override
    public void config() {
        System.out.println("This is from ImplementingAbstraction");
    }
}

public class InterfaceInJava {
    public static void main(String[] args) {

        ImplementingAbstraction ia = new ImplementingAbstraction();
        ia.config();
        ia.show();
        ia.dontShow();

        ImplementingInterface ii = new ImplementingInterface();
        ii.config();
        ii.show();

        System.out.println("The age from Interface Implementation : "+ii.age);
        System.out.println("The city from Interface Implementation : "+ii.city);

        System.out.println("The city from Interface: "+ExampleForInterface.city);
        System.out.println("The age from Interface: "+ExampleForInterface.age);

    }
}
