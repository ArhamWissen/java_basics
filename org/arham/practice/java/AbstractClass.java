package org.arham.practice.java;

abstract class Car{
    public void drive(){
        System.out.println("Driving now");
    }
    public void music(){
        System.out.println("Playing music through music system");
    }
    public abstract void engine();
}

class PetrolCar extends Car{
    @Override
    public void engine(){
        System.out.println("This is a petrol engine car");
    }
}

class DieselCar extends Car{
    @Override
    public void engine(){
        System.out.println("This is a diesel engine car");
    }
}

public class AbstractClass {

    public static void main(String[] args) {

        Car dc = new DieselCar();
        dc.engine();
        dc.drive();
        dc.music();

        Car pc = new PetrolCar();
        pc.engine();
        pc.drive();
        pc.music();

    }
}
