package org.arham.practice.java;

public class LogicalOperators {

    public static void main(String[] args) {

        int x = 7;
        int y = 5;
        int a = 5;
        int b = 9;

        boolean result1 = x>y && a>b;
        boolean result2 = x>y || a>b;
        boolean result3 = !(a>b);
        boolean result4 = !(x>y);

        System.out.println("The result 1 is : "+result1);
        System.out.println("The result 2 is : "+result2);
        System.out.println("The result 3 is : "+result3);
        System.out.println("The result 4 is : "+result4);
    }
}
