package org.arham.practice.java;

class Student{
    int rollno;
    String name;
    int marks;
}

public class Array {
    public static void main(String[] args) {

        Student s1 = new Student();
        Student s2 = new Student();
        Student s3 = new Student();

        s1.rollno = 1;
        s1.name="Arham";
        s1.marks=67;

        s2.rollno = 2;
        s2.name="Mohammed";
        s2.marks=97;

        s3.rollno = 3;
        s3.name="Mir";
        s3.marks=67;

        Student students[] = new Student[3];
        students[0] = s1;
        students[1] = s2;
        students[2] = s3;

        for(int i =0;i<students.length;i++){
            System.out.println("The name of the student is : "+students[i].name);
            System.out.println("The roll.no of the student is : "+students[i].rollno);
            System.out.println("The marks of the student is : "+students[i].marks);
        }

        System.out.println();
    }
}
