package org.arham.practice.java;

public class Operators {
    public static void main(String[] args) {
        int num1 = 7;
        int num2 = 5;

        int mul = num1*num2;
        System.out.println("The multiplication value is "+mul);

        int modResult= num1%num2;
        System.out.println("The modulus value is "+modResult);

        num1+=2;
        System.out.println("The value is after incrementation is "+num1);

        System.out.println("Incrementing the value of num1: "+(++num1));
        System.out.println("Decrementing the value of num1: "+(--num1));

//        int mul = num1*num2;
//        System.out.println("The multiplication value is "+mul);

    }
}
