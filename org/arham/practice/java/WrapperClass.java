package org.arham.practice.java;

public class WrapperClass {
    public static void main(String[] args) {


        int num1 = 8;
        System.out.println("Primitive Value : "+num1);

        Integer num2 = new Integer(8); //Boxing it, as we are doing it manually
        System.out.println("Boxing it : "+num2);

        Integer num3 = num1; // AutoBoxing it, as we are converting this automatically.
        System.out.println("Auto-Boxing it : "+num3);

        int num4 = num2.intValue(); //Unboxing it, as we are doing it manually.
        System.out.println("UnBoxing it : "+num4);

        int num5 = num2; //Auto-Unboxing it, as we are converting it automatically
        System.out.println("Auto-UnBoxing it : "+num5);

        String str = "12";
        System.out.println("Directly printing the string value : "+str);

        int num6 = Integer.parseInt(str);
        System.out.println("Converting a value from string to integer : "+num6);
    }
}
