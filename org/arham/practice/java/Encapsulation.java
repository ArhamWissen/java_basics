package org.arham.practice.java;

class Human{
    private int age;
    private String name;

    public int getAge() {
        return age;
    }
    public String getName() {
        return name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

}

public class Encapsulation {
    public static void main(String[] args) {

        Human arham = new Human();
        arham.setAge(25);
        arham.setName("Arham");

        System.out.println(arham.getName()+" : "+arham.getAge());

    }
}
