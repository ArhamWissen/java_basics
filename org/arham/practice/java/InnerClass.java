package org.arham.practice.java;


class OuterClass{
    int age;
    public void show(){
        System.out.println("Inside the show method of outer class");
    }
   static class InsideOuterClass{
        public void config(){
            System.out.println("Inside the config method of the inner class ");
        }
    }
}

public class InnerClass {
    public static void main(String[] args) {

        OuterClass oc = new OuterClass();
        oc.show();

//        OuterClass.InsideOuterClass ic = oc.new InsideOuterClass();
//        ic.config();

        OuterClass.InsideOuterClass ic = new OuterClass.InsideOuterClass();
        ic.config();
    }
}
