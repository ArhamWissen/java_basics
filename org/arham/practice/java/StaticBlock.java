package org.arham.practice.java;

class MobileForStaticBlock {
    String brand;
    int price;
    String network;
    static String name;

    static{
        name = "Phone";
        System.out.println("In static block..");
    }

    public MobileForStaticBlock(){
        System.out.println("In constructor..");
        brand = "";
        price = 200;
    }

    public void show(){
        System.out.println("The brand is : "+brand);
        System.out.println("The price is : "+price);
        System.out.println("The network is : "+network);
        System.out.println("The name is : "+name);
    }
}
public class StaticBlock {
    public static void main(String[] args) throws ClassNotFoundException {

        Class.forName("MobileForStaticBlock");

//        MobileForStaticBlock obj1 = new MobileForStaticBlock();
//        MobileForStaticBlock obj2 = new MobileForStaticBlock();
//        obj1.brand="Apple";
//        obj1.price=1500;
//        System.out.println();
    }
}
