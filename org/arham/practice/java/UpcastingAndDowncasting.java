package org.arham.practice.java;

class First{
    public void show1(){
        System.out.println("Inside the show of First Class");
    }
}
class Second extends First{
    public void show2(){
        System.out.println("Inside the show of Second Class");
    }
}

public class UpcastingAndDowncasting {
    public static void main(String[] args) {

        double d = 4.5;
        int i = (int) d;

        First f1 = new First();
        // Upcasting
        First f2 = new Second();
        f1.show1();

        //DownCasting
        Second s1 = (Second) f2;
        s1.show2();

        System.out.println(d);
        System.out.println(i);
    }
}
