package org.arham.practice.java;

// When you declare a VARIABLE as final, the value of that variable cannot be changed in the future.
// When you declare a CLASS as final, that particular class cannot be extended. The main purpose of this is to stop inheritance.
// When you declare a METHOD as final, you cannot Override that particular method. The main purpose of this is to stop overriding of methods.

final class FinalCalc{
    public final void show(){
        System.out.println("In FinalCalc show ");
    }
    public int add(int a, int b){
        return a+b;
    }
}

//class FinalAdvCalc extends FinalCalc{
//    @Override
//    public void show(){
//        System.out.println("In FinalAdvCalc show ");
//    }
//    public int add(int a, int b){
//        return a+b;
//    }
//}

public class FinalKeyword {
    public static void main(String[] args) {

        final int num = 8;
//        num = 9;
        System.out.println("The final and constant value of num is = "+num);

        FinalCalc fc = new FinalCalc();
//        FinalAdvCalc fac = new FinalAdvCalc();
        System.out.println(fc.add(4,5));
    }
}
