package org.arham.practice.java;

public class Data_Types {

    public static void main(String []args){
//        Integer values
        int num1 = 3;
        int num2 = 3;
        int sum = num1+num2;
        System.out.println("The sum = "+sum);

//        Byte value
        byte by = 127;

//        Short value
        short sh = 558;

//        Long value
        long lg = 5000L;

//        Float value
        float f = 5.8f;

//      Character value
        char c = 'A';

//        Boolean value
        boolean bool = true;


        System.out.println("Just testing out the data types");

    }
}
