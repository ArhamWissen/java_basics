package org.arham.practice.java;


abstract class A_AnonymousClass{
    public abstract void show();
}

class B_AnonymousClass extends A_AnonymousClass{
    @Override
    public void show(){
        System.out.println("Inside the B class of Anonymous Class");
    }
}

public class AnonymousInnerClass {
    public static void main(String[] args) {

//        A_AnonymousClass obj = new A_AnonymousClass(){
//            public void show(){
//                System.out.println("Inside the new anonymous class");
//            }
//        };
//        obj.show();

        A_AnonymousClass obj = new A_AnonymousClass(){
            public void show(){
                System.out.println("In new show");
            }
        };
        obj.show();
    }
}
