package org.arham.practice.java;

public class StringBufferandStringBuilder {

    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("Mir Mohammed Arham ");
        sb.append("Engineer");

        System.out.println(sb);
        System.out.println(sb.capacity());
        System.out.println(sb.length());
    }
}
