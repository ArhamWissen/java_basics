package org.arham.practice.java;


import java.util.Arrays;

enum Status{
    Running,Failed,Success,Pending
        }
public class Enum {
    public static void main(String[] args) {

//        Status s = Status.Running;
//
//        Status [] s1 = Status.values();
//        System.out.println("The value of s is : "+s);
//
//        for (Status s2:s1) {
//            System.out.println(s2);
//        }

        Status s = Status.Pending;

        switch (s){
            case Running -> System.out.println("All Good");
            case Pending -> System.out.println("Please wait");
            case Success -> System.out.println("Done");
            case Failed -> System.out.println("Failed");
        }

        if(s == Status.Running){
            System.out.println("All Good");
        }
        else if(s == Status.Failed){
            System.out.println("Failed");
        }
        else if(s == Status.Pending){
            System.out.println("Please wait");
        }
        else if(s == Status.Success){
            System.out.println("Done");
        }

    }
}
