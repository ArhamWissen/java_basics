package org.arham.practice.java;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public class StreamAPI {
    public static void main(String[] args) {

        List<Integer> nums = Arrays.asList(4,7,8,5,2,3);

//        Stream<Integer> s1 = nums.stream();
//        Stream<Integer> s2 = s1.filter(n -> n%2 ==0);
//        Stream<Integer> s3 = s2.map(n-> n*2);
//
//        s3.forEach(n-> System.out.println(n));

//        Function<Integer,Integer> fun = n -> n*2;


//        int result = nums.stream().filter(n->n%2 == 0).map(n->n*2).reduce(0,(c,e) -> c+e);
//        System.out.println(result);
//
//        Stream<Integer> sortedValues = nums.stream().filter(n-> n%2==0).sorted();
//        System.out.println(sortedValues);
        nums.stream().filter(n-> n%2==0).sorted().forEach(System.out::println);



    }
}
