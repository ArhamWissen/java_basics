package org.arham.practice.java;

class HumanConstructor{
    private int age;
    private String name;

    public HumanConstructor(int age, String name){
        this.age = age;
        this.name = name;
        System.out.println("Inside the constructor");
    }
    public HumanConstructor(){
        age = 12;
        name = "ArhamInsideAConstructor";
        System.out.println("Inside the constructor");
    }

    public int getAge() {
        return age;
    }
    public String getName() {
        return name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

}

public class Constructors {
    public static void main(String[] args) {

        HumanConstructor arham = new HumanConstructor(18,"Arham");

        System.out.println(arham.getName()+" : "+arham.getAge());

    }
}
