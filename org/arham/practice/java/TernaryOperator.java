package org.arham.practice.java;

public class TernaryOperator {
    public static void main(String[] args) {


        int n = 5;
        int result = 0;

        int result1 = n%2==0?10:5;
        int result2 = n%2!=0?10:5;

        System.out.println("The value of result is = "+result1);
        System.out.println("The value of result is = "+result2);
    }
}
