package org.arham.practice.java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TryWithResources {
    public static void main(String[] args) throws IOException {
        int num = 0;


        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
//            This approach will close the method automatically.
//            Directly initializing the buffered reader along with the try block.
//            This is also called try with resources.
            num = Integer.parseInt(br.readLine());
            System.out.println(num);
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
//        finally {
//            br.close();
//            System.out.println("Bye!!!");
//        }
    }
}
