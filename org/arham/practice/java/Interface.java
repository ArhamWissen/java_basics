package org.arham.practice.java;

interface ComputerForDeveloper{
    void code();
}

class LaptopForDeveloper implements ComputerForDeveloper{
    public void code(){
        System.out.println("Code compile and run...");
    }
}

class DesktopForDeveloper implements ComputerForDeveloper{
    public void code(){
        System.out.println("Faster Coding, Faster Compiling and Faster Execution...");
    }
}

class Developer{
    public void DevApp(ComputerForDeveloper comp){
        System.out.println("I am coding...");
        comp.code();
    }
}

public class Interface {

    public static void main(String[] args) {


        ComputerForDeveloper lap = new LaptopForDeveloper();
        ComputerForDeveloper desk = new DesktopForDeveloper();

        Developer arham = new Developer();
        arham.DevApp(desk);

    }
}
